package com.epita.socra.app;

import com.epita.socra.app.tools.*;

/**
 * Hello world!
 */
public final class App {
    private IOAdapter adapter;

    private App() {
        this(new ConsoleAdapter());
    }

    public App(IOAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * Says hello to the world.
     * 
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        App application = new App();
        application.run();
    }

    //run application which can be improve in the future
    public void run(){
        adapter.write("Welcome !");
        choose_mode();
    }

    public void choose_mode()
    {
        adapter.write("Choose mode :");
        String number = adapter.read();

        if (number.equals("Roman"))
            iter_roman();
        else if (number.equals("Arabic"))
            iter_arabic();
        else
            adapter.write("Wrong mode");
    }

    public void iter_roman()
    {
        adapter.write("roman > ");
        String roman = adapter.read();
        while (roman != "exit")
        {
            StringBuilder res = new StringBuilder();
            res.append(Converter.convert_arabic(roman));
            adapter.write(res.toString());
            adapter.write("roman > ");
            roman = adapter.read();
        }
    }

    public void iter_arabic()
    {
        adapter.write("arabic > ");
        String arabic = adapter.read();
        while (arabic != "exit")
        {
            //check if input is an int
            int new_arabic;
            try {
                new_arabic = Integer.parseInt(arabic);
            } catch (Exception e)
            {
                adapter.write("Wrong format");
                adapter.write("arabic > ");
                arabic = adapter.read();
                continue;
            }

            StringBuilder res = new StringBuilder();
            res.append(Converter.convert_roman(new_arabic));
            adapter.write(res.toString());
            adapter.write("arabic > ");
            arabic = adapter.read();
        }
    }
}
