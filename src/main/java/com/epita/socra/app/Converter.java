package com.epita.socra.app;

import javax.swing.*;
import java.util.ArrayList;

public class Converter {

    //list of roman number from 0 to 1000
    static String[] roman = {"I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"};

    //list of arabic number from 0 to 1000
    static int[] arabic = {1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000};

    //convert arabic number to roman number
    public static String convert_roman(int number) {

        StringBuilder res = new StringBuilder();

        for (int i = roman.length - 1; i >= 0; i--) {
            while (number >= arabic[i]) {
                res.append(roman[i]);
                number -= arabic[i];
            }
        }
        return new String(res);
    }

    //convert arabic number to roman number
    public static int convert_arabic(String number) {

        int res = 0;
        int cursor = 0;
        int roman_index = roman.length - 1;

        while (cursor < number.length() && roman_index >= 0)
        {
            //check roman number with length = 2
            if (roman[roman_index].length() == 2 && cursor < number.length() - 1) {
                //tmp is a substring of roman number
                StringBuilder tmp = new StringBuilder();
                tmp.append(number.charAt(cursor));
                tmp.append(number.charAt(cursor + 1));

                String s = new String(tmp);

                if (s.equals(roman[roman_index]))
                {
                    res += (arabic[roman_index]);
                    cursor += 2;
                }
            }
            // check roman number with length = 1
            if (roman[roman_index].length() == 1
                    && roman[roman_index].charAt(0) == (number.charAt(cursor)))
            {
                res += arabic[roman_index];
            }
            else
            {
                roman_index -= 1;
                continue;
            }
            cursor += 1;
        }
        return res;
    }
}
