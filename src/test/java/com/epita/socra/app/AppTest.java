package com.epita.socra.app;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

import com.epita.socra.app.tools.IOAdapter;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test.
     */
    @Test
    public void givenAMock_WhenRunningMain_ThenCheckOuputs() {
        IOAdapter mock = mock(IOAdapter.class);

        when(mock.read()).thenReturn("Roman", "exit");
        App app = new App(mock);
        app.run();

        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(1)).write("roman > ");
    }

    @Test
    public void input_roman_10()
    {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("Roman", "X", "exit");
        App app = new App(mock);
        app.run();
        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(2)).write("roman > ");
        verify(mock, times(1)).write("10");
        return;
    }

    @Test
    public void input_roman_10_and_42()
    {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("Roman", "X", "XLII", "exit");
        App app = new App(mock);
        app.run();
        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(3)).write("roman > ");
        verify(mock, times(1)).write("10");
        verify(mock, times(1)).write("42");
        return;
    }

    @Test
    public void input_arabic_10()
    {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("Arabic", "10", "exit");
        App app = new App(mock);
        app.run();
        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(2)).write("arabic > ");
        verify(mock, times(1)).write("X");
        return;
    }

    @Test
    public void input_arabic_10_and_42()
    {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("Arabic", "10", "42", "exit");
        App app = new App(mock);
        app.run();
        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(3)).write("arabic > ");
        verify(mock, times(1)).write("X");
        verify(mock, times(1)).write("XLII");
        return;
    }

    @Test
    public void input_mode_false()
    {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("toto");
        App app = new App(mock);
        app.run();

        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(1)).write("Wrong mode");
    }

    @Test
    public void input_arabic_false()
    {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("Arabic", "toto", "exit");
        App app = new App(mock);
        app.run();

        verify(mock, times(1)).write("Welcome !");
        verify(mock, times(1)).write("Choose mode :");
        verify(mock, times(2)).write("arabic > ");
        verify(mock, times(1)).write("Wrong format");
    }
}
