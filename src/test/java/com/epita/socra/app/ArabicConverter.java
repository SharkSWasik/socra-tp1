package com.epita.socra.app;

import org.junit.Test;

import javax.print.DocFlavor;

import static org.junit.Assert.assertEquals;

public class ArabicConverter {

   @Test
   public void convert_One_arabic() {
      String one = "I";
      int res = Converter.convert_arabic(one);

      assertEquals(1, res);
   }
   @Test
   public void convert_five_arabic() {
      String five = "V";
      int res = Converter.convert_arabic(five);

      assertEquals(5, res);
   }
   @Test
   public void convert_ten_arabic() {
      String ten = "X";
      int res = Converter.convert_arabic(ten);

      assertEquals(10, res);
   }

   @Test
   public void convert_fifty_arabic() {
      String fifty = "L";
      int res = Converter.convert_arabic(fifty);

      assertEquals(50, res);
   }

   @Test
   public void convert_hundred_arabic() {
      String hundred = "C";
      int res = Converter.convert_arabic(hundred);

      assertEquals(100, res);
   }

   @Test
   public void convert_1000_arabic() {
      String mille = "M";
      int res = Converter.convert_arabic(mille);

      assertEquals(1000, res);
   }

   @Test
   public void convert_500_arabic() {
      String cinqcent = "D";
      int res = Converter.convert_arabic(cinqcent);

      assertEquals(500, res);
   }


   @Test
   public void convert_42_arabic() {
      int res = Converter.convert_arabic("XLII");

      assertEquals(42, res);
   }

   @Test
   public void convert_1903_arabic() {
      int res = Converter.convert_arabic("MCMIII");

      assertEquals(1903, res);
   }

   @Test
   public void convert_999_roman() {
      int res = Converter.convert_arabic("CMXCIX");

      assertEquals(999, res);
   }

   @Test
   public void convert_83_roman() {
      int res = Converter.convert_arabic("LXXXIII");

      assertEquals(83, res);
   }
}
