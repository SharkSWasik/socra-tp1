package com.epita.socra.app;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanConverter {

    @Test
    public void convert_One_roman() {
        int one = 1;
        String res = Converter.convert_roman(one);

        assertEquals("I", res);
    }
    @Test
    public void convert_five_roman() {
        int five = 5;
        String res = Converter.convert_roman(five);

        assertEquals("V", res);
    }

    @Test
    public void convert_ten_roman() {
        int ten = 10;
        String res = Converter.convert_roman(ten);

        assertEquals("X", res);
    }

    @Test
    public void convert_fifty_roman() {
        int fifty = 50;
        String res = Converter.convert_roman(fifty);

        assertEquals("L", res);
    }

    @Test
    public void convert_hundred_roman() {
        int hundred = 100;
        String res = Converter.convert_roman(hundred);

        assertEquals("C", res);
    }

    @Test
    public void convert_1000_roman() {
        int mille = 1000;
        String res = Converter.convert_roman(mille);

        assertEquals("M", res);
    }

    @Test
    public void convert_500_roman() {
        int cinqcent = 500;
        String res = Converter.convert_roman(cinqcent);

        assertEquals("D", res);
    }

    @Test
    public void convert_42_roman() {
        String res = Converter.convert_roman(42);

        assertEquals("XLII", res);
    }

    @Test
    public void convert_1903_roman() {
        String res = Converter.convert_roman(1903);

        assertEquals("MCMIII", res);
    }

    @Test
    public void convert_999_roman() {
        String res = Converter.convert_roman(999);

        assertEquals("CMXCIX", res);
    }

    @Test
    public void convert_83_roman() {
        String res = Converter.convert_roman(83);

        assertEquals("LXXXIII", res);
    }
}
